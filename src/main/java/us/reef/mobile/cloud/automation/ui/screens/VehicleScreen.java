package us.reef.mobile.cloud.automation.ui.screens;

import io.appium.java_client.MobileBy;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import us.reef.mobile.cloud.automation.exceptions.MobileAutomationException;
import us.reef.mobile.cloud.automation.storage.VehicleEntity;
import us.reef.mobile.cloud.automation.ui.base.BaseDriver;
import us.reef.mobile.cloud.automation.ui.base.BasePage;

import java.util.ArrayList;
import java.util.List;

import static io.cucumber.spring.CucumberTestContext.SCOPE_CUCUMBER_GLUE;

@Slf4j
@Component
@Scope(SCOPE_CUCUMBER_GLUE)
public class VehicleScreen extends BasePage {

    final By vehicleScreenId = MobileBy.AccessibilityId("AddVehicleScreen_HederTitle");
    final By carNickName = MobileBy.AccessibilityId("AddVehicleCell_TextField0");
    final By licensePlateNumber = MobileBy.AccessibilityId("AddVehicleCell_TextField1");
    final By licensePlateState = MobileBy.AccessibilityId("AddVehicleCell_SelectStateButton");
    final By licensePlateStateWheel = By.xpath("//*[@type='XCUIElementTypePickerWheel']");
    final By oversizedVehicle = MobileBy.AccessibilityId("OversizedFieldCell_CheckButton");
    final By oversizedVehicleInfo = MobileBy.AccessibilityId("More Info");
    final By saveButton = MobileBy.AccessibilityId("AddVehicleScreen_ContinueButton");

    public VehicleScreen(final BaseDriver baseDriver) {
        super(baseDriver);
    }

    /**
     * Check is Vehicle screen displayed.
     *
     * @return true if it is displayed, otherwise false.
     */
    public boolean isVehicleScreenDisplayed() {
        log.info("Checking is Vehicle screen displayed.");
        return waitForElementToBeDisplayed(vehicleScreenId);
    }

    /**
     * User fills in Vehicle form with data.
     *
     * @param carNickName        Car Nick Name.
     * @param licensePlateNumber License Plate Number.
     * @param licensePlateState  License Plate State.
     */
    public void fillInVehicleForm(final String carNickName,
                                  final String licensePlateNumber,
                                  final String licensePlateState) {
        waitAndSendKeys(this.carNickName, carNickName);
        waitAndSendKeys(this.licensePlateNumber, licensePlateNumber);
        // Not able to find AccessibilityId for osVersion 14 on BrowserStack, check from time to time if it works with id
        if (getOsVersion().equals("14")) {
            tapElement(By.xpath("//XCUIElementTypeApplication[@name=\"REEF Mobile\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[3]"));
        } else {
            tapElement(this.licensePlateState);
        }
        waitAndSendKeys(this.licensePlateStateWheel, licensePlateState);
        log.info("Entered Car Nick name: {}, License Plate Number: {}, License Plate State: {}.",
                carNickName, licensePlateNumber, licensePlateState);
        tapKeyboardDoneButton();
        tapKeyboardDoneButton();
        final List<VehicleEntity> vehicle = new ArrayList<>();
        final VehicleEntity vehicleEntity = VehicleEntity.builder()
                .carNickName(carNickName)
                .licensePlateNumber(licensePlateNumber)
                .licensePlateState(licensePlateState)
                .build();
        vehicle.add(vehicleEntity);
        getStorage().setVehicles(vehicle);
        log.info("Vehicle added successfully.");
    }

    /**
     * Tap on Save button.
     */
    public void tapOnSaveButton() {
        if (driver.findElement(saveButton).getAttribute("enabled").equals("true")) {
            waitAndTap(saveButton);
            log.info("Tapped on Save button.");
        } else {
            throw new MobileAutomationException("Save button is not enabled!");
        }
    }
}