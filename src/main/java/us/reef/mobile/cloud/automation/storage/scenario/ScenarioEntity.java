package us.reef.mobile.cloud.automation.storage.scenario;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class ScenarioEntity {
    private String scenarioName;
}
