package us.reef.mobile.cloud.automation.rest.data.graphql.responses.verification;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import us.reef.mobile.cloud.automation.rest.data.graphql.responses.common.CreditCard;
import us.reef.mobile.cloud.automation.rest.data.graphql.responses.common.Phone;
import us.reef.mobile.cloud.automation.rest.data.graphql.responses.common.ResponseStatus;
import us.reef.mobile.cloud.automation.rest.data.graphql.responses.common.Vehicle;

import java.util.List;

/**
 * @author Nikola Komazec (nikola.komazec@htecgroup.com)
 * Created on: 09/11/2020
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class VerifyOtpResponse {
    @JsonProperty("data")
    private VerifyOtp verifyOtp;

    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    public static class VerifyOtp {
        @JsonProperty("verifyOTP")
        private VerifyOtpData verifyOtpData;
    }

    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    public static class VerifyOtpData {
        private String emailAddress;
        private Phone phone;
        private List<Vehicle> vehicles;
        private List<CreditCard> creditCards;
        private ResponseStatus responseStatus;
        private String accessToken;
    }
}