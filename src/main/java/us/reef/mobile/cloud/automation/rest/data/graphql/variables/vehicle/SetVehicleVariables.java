package us.reef.mobile.cloud.automation.rest.data.graphql.variables.vehicle;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Setter;
import us.reef.mobile.cloud.automation.rest.data.graphql.variables.common.AppVersionVariables;
import us.reef.mobile.cloud.automation.rest.data.graphql.variables.common.VehicleVariables;

/**
 * @author Nikola Komazec (nikola.komazec@htecgroup.com)
 * Created on: 09/11/2020
 */
@Setter
public class SetVehicleVariables {
    @SerializedName("setVehicleInput")
    @Expose
    private SetVehicleInputDataVariables setVehicleInputDataVariables;

    @Setter
    public static class SetVehicleInputDataVariables {
        @SerializedName("action")
        @Expose
        private int action;
        @SerializedName("vehicle")
        @Expose
        private VehicleVariables vehicleVariables;
        @SerializedName("appVersion")
        @Expose
        private AppVersionVariables appVersionVariables;
    }


}
