package us.reef.mobile.cloud.automation.rest.data.graphql.variables.common;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Setter;

/**
 * @author Nikola Komazec (nikola.komazec@htecgroup.com)
 * Created on: 09/11/2020
 */
@Setter
public class PhoneVariables {
    @SerializedName("mobilePhoneCountryCode")
    @Expose
    private String mobilePhoneCountryCode;
    @SerializedName("mobilePhoneNumber")
    @Expose
    private String mobilePhoneNumber;
}
