package us.reef.mobile.cloud.automation.storage.testrail;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class TestRailEntity {
    private List<Integer> caseIds;
}
