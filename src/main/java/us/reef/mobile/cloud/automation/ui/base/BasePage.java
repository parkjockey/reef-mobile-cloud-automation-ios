package us.reef.mobile.cloud.automation.ui.base;

import io.appium.java_client.MobileBy;
import io.appium.java_client.TouchAction;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.ios.IOSElement;
import io.appium.java_client.touch.offset.ElementOption;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Value;
import us.reef.mobile.cloud.automation.storage.Storage;

@Slf4j
@Getter
public abstract class BasePage {

    protected final IOSDriver<IOSElement> driver;
    private final WebDriverWait webDriverWait;
    private final Storage storage;
    @Value("${bs.osVersion}")
    public String osVersion;
    @Value("${bs.mobilePhone}")
    public String mobilePhone;

    public BasePage(final BaseDriver baseDriver) {
        this.driver = baseDriver.getDriver();
        this.webDriverWait = new WebDriverWait(driver, 10);
        this.storage = new Storage();
    }

    /**
     * This method creates a build instance of the {@link ElementOption}.
     *
     * @param element is the element to calculate offset from.
     * @return the built option
     */
    public static ElementOption element(final WebElement element) {
        return new ElementOption().withElement(element);
    }

    /**
     * Long press an element and move to another element.
     *
     * @param element1 main element
     * @param element2 move to second element
     */
    public void longPressAndMoveTo(final By element1, final By element2) {
        final TouchAction<?> dragNDrop = new TouchAction<>(driver);
        dragNDrop.longPress(element(driver.findElement(element1)))
                .moveTo(element(driver.findElement(element2)))
                .release();
        dragNDrop.perform();
    }

    /**
     * Wait for element to be displayed and send keys to it
     *
     * @param by         element on the page
     * @param keysToSend value which we send to the element
     */
    public void waitAndSendKeys(final By by, final String keysToSend) {
        waitForElementToBeDisplayed(by);
        driver.findElement(by).sendKeys(keysToSend);
    }

    /**
     * Send keys to element
     *
     * @param by         element on the page
     * @param keysToSend value which we send to the element
     */
    public void sendKeys(final By by, final String keysToSend) {
        driver.findElement(by).sendKeys(keysToSend);
    }

    /**
     * Wait for element to be displayed and click on it
     *
     * @param by element to be waited for and clicked
     */
    public void waitAndTap(final By by) {
        waitForElementToBeDisplayed(by);
        driver.findElement(by).click();
    }

    /**
     * Click Element.
     *
     * @param by element to be clicked
     */
    public void tapElement(final By by) {
        driver.findElement(by).click();
    }

    /**
     * Wait for element to be displayed
     *
     * @param by element to be waited for
     * @return true if element is displayed otherwise false
     */
    public boolean waitForElementToBeDisplayed(final By by) {
        return getWebDriverWait().until(ExpectedConditions.visibilityOfElementLocated(by)).isDisplayed();
    }

    /**
     * Check is element present and displayed on the page
     *
     * @param by element to bee checked
     * @return true if it is displayed and present otherwise false
     */
    public boolean isElementPresentAndDisplayed(final By by) {
        try {
            return driver.findElement(by).isDisplayed();
        } catch (final NoSuchElementException e) {
            return false;
        }
    }

    /**
     * Tap on Done button.
     */
    public void tapKeyboardDoneButton() {
        waitAndTap(MobileBy.AccessibilityId("Done"));
    }

    /**
     * Tap numbers on keyboard.
     *
     * @param numbers numbers to be tapped on keyboard.
     */
    public void tapKeyboardNumbers(final String numbers) {
        final int numberOfCharacters = numbers.length();
        for (int i = 0; numberOfCharacters > i; i++) {
            driver.findElement(MobileBy.AccessibilityId(String.valueOf(numbers.charAt(i)))).click();
        }
    }
}