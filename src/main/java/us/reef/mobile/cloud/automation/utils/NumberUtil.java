package us.reef.mobile.cloud.automation.utils;

import lombok.experimental.UtilityClass;

import java.util.Random;

@UtilityClass
public class NumberUtil {

    /**
     * Generate valid phone number.
     *
     * @return Valid Phone Number.
     */
    public String generateValidPhoneNumber() {
        final int randomFirstNumber = new Random().nextInt(999 - 100) + 100;
        final int randomSecondNumber = new Random().nextInt(999 - 100) + 100;
        return "4044" + randomFirstNumber + "" + randomSecondNumber;
    }

    /**
     * Generate invalid phone number.
     *
     * @return Invalid Phone Number.
     */
    public String generateInvalidPhoneNumber() {
        final int randomFirstNumber = new Random().nextInt(999 - 100) + 100;
        final int randomSecondNumber = new Random().nextInt(999 - 100) + 100;
        return "000" + randomFirstNumber + "" + randomSecondNumber;
    }
}