package us.reef.mobile.cloud.automation.storage;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class UserEntity {
    private String phoneNumber;
    private String email;
}
