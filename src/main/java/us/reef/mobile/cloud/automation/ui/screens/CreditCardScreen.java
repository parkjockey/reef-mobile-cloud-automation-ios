package us.reef.mobile.cloud.automation.ui.screens;

import io.appium.java_client.MobileBy;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import us.reef.mobile.cloud.automation.rest.data.creditcard.CreditCard;
import us.reef.mobile.cloud.automation.storage.CreditCardEntity;
import us.reef.mobile.cloud.automation.ui.base.BaseDriver;
import us.reef.mobile.cloud.automation.ui.base.BasePage;

import static io.cucumber.spring.CucumberTestContext.SCOPE_CUCUMBER_GLUE;

/**
 * @author Nikola Komazec (nikola.komazec@htecgroup.com)
 * Created on: 27/11/2020
 */
@Slf4j
@Component
@Scope(SCOPE_CUCUMBER_GLUE)
public class CreditCardScreen extends BasePage {

    final By creditCardPageId = MobileBy.AccessibilityId("Pay Parking Notices");
    final By cardNumber = By.xpath("//XCUIElementTypeOther[@name=\"Pay Parking Notices\"]/XCUIElementTypeTextField[1]");
    final By cardName = By.xpath("//XCUIElementTypeOther[@name=\"Pay Parking Notices\"]/XCUIElementTypeTextField[2]");
    final By cvv = By.xpath("//XCUIElementTypeOther[@name=\"Pay Parking Notices\"]/XCUIElementTypeTextField[3]");
    final By monthSelection = By.xpath("//*[@value='Month']");
    final By monthSelectionV14 = By.xpath("//XCUIElementTypeOther[@name=\"Pay Parking Notices\"]/XCUIElementTypeOther[2]");
    final By yearSelection = By.xpath("//*[@value='Year']");
    final By yearSelectionV14 = By.xpath("//XCUIElementTypeOther[@name=\"Pay Parking Notices\"]/XCUIElementTypeOther[3]");
    final By wheelPicker = By.xpath("//XCUIElementTypeApplication[@name=\"REEF Mobile\"]/XCUIElementTypeWindow[4]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypePicker");
    final By wheelPickerV13 = By.xpath("//XCUIElementTypeApplication[@name=\"REEF Mobile\"]/XCUIElementTypeWindow[3]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypePicker");
    final By addCreditCardButton = By.xpath("//*[@name='Add Credit Card']");

    final By doneButton = MobileBy.AccessibilityId("Done");

    public CreditCardScreen(final BaseDriver baseDriver) {
        super(baseDriver);
    }

    /**
     * Check is credit card screen displayed.
     *
     * @return true if it is displayed, otherwise false.
     */
    public boolean isCreditCardScreenDisplayed() {
        log.info("Checking is Credit Card screen displayed.");
        return waitForElementToBeDisplayed(creditCardPageId);
    }

    /**
     * Add test credit card by a specific card provider.
     *
     * @param cardProvider Card Provider (Visa, Master Card, American Express, Discover)
     */
    public void addTestCreditCard(final String cardProvider) {
        final CreditCard creditCardNumber;
        final String fullCardName = "Automated Test";
        final String cvvNumber = "357";
        creditCardNumber = CreditCard.getCreditCardNumber(cardProvider.toUpperCase());
        waitAndSendKeys(this.cardNumber, creditCardNumber.card);
        log.info("Entered credit card number {}.", creditCardNumber.card);
        tapOnDoneButtonIfItIsDisplayed();
        waitAndSendKeys(this.cardName, fullCardName);
        log.info("Entered Card name {}.", fullCardName);
        tapOnDoneButtonIfItIsDisplayed();
        waitAndSendKeys(this.cvv, cvvNumber);
        tapOnDoneButtonIfItIsDisplayed();
        log.info("Entered CVV number {}.", cvvNumber);
        if (getOsVersion().contains("14")) {
            waitAndTap(monthSelectionV14);
        } else {
            waitAndTap(monthSelection);
        }
        if (getOsVersion().contains("13") || getOsVersion().contains("14")) {
            longPressAndMoveTo(wheelPickerV13, doneButton);
        } else {
            longPressAndMoveTo(wheelPicker, doneButton);
        }
        log.info("Selected month.");
        tapOnDoneButtonIfItIsDisplayed();
        if (getOsVersion().contains("14")) {
            waitAndTap(yearSelectionV14);
        } else {
            waitAndTap(yearSelection);
        }
        if (getOsVersion().contains("13") || getOsVersion().contains("14")) {
            longPressAndMoveTo(wheelPickerV13, doneButton);
        } else {
            longPressAndMoveTo(wheelPicker, doneButton);
        }
        log.info("Selected year.");
        tapOnDoneButtonIfItIsDisplayed();
        waitAndTap(addCreditCardButton);
        log.info("Tapped on add credit card button.");
        final CreditCardEntity card = CreditCardEntity.builder()
                .cardNumber(creditCardNumber.card)
                .cardName(fullCardName)
                .cvv(cvvNumber)
                .build();
        getStorage().getCreditCards().add(card);
    }

    private void tapOnDoneButtonIfItIsDisplayed() {
        if (isElementPresentAndDisplayed(doneButton)) {
            waitAndTap(doneButton);
        }
    }
}