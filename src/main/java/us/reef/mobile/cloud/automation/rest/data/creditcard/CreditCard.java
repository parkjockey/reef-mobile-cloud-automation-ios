package us.reef.mobile.cloud.automation.rest.data.creditcard;

import us.reef.mobile.cloud.automation.exceptions.MobileAutomationException;

public enum CreditCard {

    VISA("4111111111111111"),
    MASTER_CARD("5500000000000004"),
    AMERICAN_EXPRESS("340000000000009"),
    DISCOVER("36438999960016");

    public final String card;

    CreditCard(final String creditCard) {
        this.card = creditCard;
    }

    /**
     * Get Credit Card Number.
     *
     * @param cardProvider Card Provider (Visa, Master Card, American Express, Discover)
     * @return CreditCard {@link CreditCard}
     */
    public static CreditCard getCreditCardNumber(final String cardProvider) {
        switch (cardProvider.toUpperCase()) {
            case "VISA":
                return CreditCard.VISA;
            case "MASTER CARD":
                return CreditCard.MASTER_CARD;
            case "AMERICAN EXPRESS":
                return CreditCard.AMERICAN_EXPRESS;
            case "DISCOVER":
                return CreditCard.DISCOVER;
            default:
                throw new MobileAutomationException("Credit card {} is not supported!", cardProvider);
        }
    }
}