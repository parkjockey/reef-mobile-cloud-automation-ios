package us.reef.mobile.cloud.automation.ui.screens;

import io.appium.java_client.MobileBy;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import us.reef.mobile.cloud.automation.exceptions.MobileAutomationException;
import us.reef.mobile.cloud.automation.ui.base.BaseDriver;
import us.reef.mobile.cloud.automation.ui.base.BasePage;
import us.reef.mobile.cloud.automation.utils.NumberUtil;
import us.reef.mobile.cloud.automation.utils.TextUtil;

import static io.cucumber.spring.CucumberTestContext.SCOPE_CUCUMBER_GLUE;

@Slf4j
@Component
@Scope(SCOPE_CUCUMBER_GLUE)
public class WelcomeScreen extends BasePage {

    final By welcomeScreenId = MobileBy.AccessibilityId("EnterPhoneNumberScreen_HederTitle");

    final By phoneNumber = MobileBy.AccessibilityId("EnterPhoneNumberScreen_PhoneNumberTextField");
    final By email = MobileBy.AccessibilityId("EnterPhoneNumberScreen_EmailTextField");
    final By licensesAgreement = MobileBy.AccessibilityId("EnterPhoneNumberScreen_CheckAgreementButton");
    final By marketingInformation = MobileBy.AccessibilityId("EnterPhoneNumberScreen_CheckMarketingInfoButton");
    final By continueButton = MobileBy.AccessibilityId("EnterPhoneNumberScreen_ContinueButton");

    public WelcomeScreen(final BaseDriver baseDriver) {
        super(baseDriver);
    }

    /**
     * Check is Welcome screen displayed by validating that "Enter your phone number to sign up or log in." text is displayed.
     *
     * @return true if it is displayed, otherwise false.
     */
    public boolean isWelcomeScreenDisplayed() {
        log.info("Checking is Welcome screen displayed.");
        return waitForElementToBeDisplayed(welcomeScreenId);
    }

    /**
     * Enter valid or invalid Phone Number.
     *
     * @param valid or invalid phone number.
     */
    public void enterPhoneNumber(final boolean valid) {
        waitAndTap(phoneNumber);
        if (valid) {
            final String validPhoneNumber = NumberUtil.generateValidPhoneNumber();
            tapKeyboardNumbers(validPhoneNumber);
            log.info("Entered Valid Phone Number {}.", validPhoneNumber);
            getStorage().getUser().setPhoneNumber(validPhoneNumber);
        } else {
            final String invalidPhoneNumber = NumberUtil.generateInvalidPhoneNumber();
            tapKeyboardNumbers(invalidPhoneNumber);
            log.info("Entered Invalid Phone Number {}.", invalidPhoneNumber);
        }
        tapKeyboardDoneButton();
    }

    /**
     * Enter valid or invalid Email.
     *
     * @param valid or invalid email.
     */
    public void enterEmail(final boolean valid) {
        if (valid) {
            final String validEmail = TextUtil.getValidEmail();
            waitAndSendKeys(email, validEmail);
            log.info("Entered Valid Email {}.", validEmail);
            getStorage().getUser().setEmail(validEmail);
        } else {
            final String invalidEmail = TextUtil.getInvalidEmail();
            waitAndSendKeys(email, invalidEmail);
            log.info("Entered Invalid Email {}.", invalidEmail);
        }
        tapKeyboardDoneButton();
    }

    /**
     * Agrees or Disagrees to License Agreement.
     *
     * @param agree if true user agrees, if false user disagrees.
     */
    public void agreeToLicenseAgreement(final boolean agree) {
        if (agree) {
            waitAndTap(licensesAgreement);
            log.info("Agrees to License Agreement.");
        } else {
            log.info("Disagrees to License Agreement.");
        }
    }

    /**
     * Agrees to receive Marketing information about products and services via SMS.
     *
     * @param agree if true user agrees, if false user disagrees.
     */
    public void agreeToMarketingInformation(final boolean agree) {
        if (agree) {
            waitAndTap(marketingInformation);
            log.info("Agrees to receive Marketing Information about products and services via SMS.");
        } else {
            log.info("Disagrees to receive Marketing Information about products and services via SMS.");
        }
    }

    /**
     * Tap on Continue button.
     */
    public void tapOnContinueButton() {
        if (driver.findElement(continueButton).getAttribute("enabled").equals("true")) {
            waitAndTap(continueButton);
            log.info("Tapped on Continue button.");
        } else {
            throw new MobileAutomationException("Continue button is not enabled!");
        }
    }
}