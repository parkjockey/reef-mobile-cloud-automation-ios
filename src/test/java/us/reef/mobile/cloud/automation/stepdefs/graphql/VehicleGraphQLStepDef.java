package us.reef.mobile.cloud.automation.stepdefs.graphql;

import io.cucumber.java.en.Given;
import org.springframework.beans.factory.annotation.Autowired;
import us.reef.mobile.cloud.automation.rest.data.graphql.responses.useraccount.vehicle.SetVehicleResponse;
import us.reef.mobile.cloud.automation.rest.proxy.graphql.VehicleProxy;

/**
 * @author Nikola Komazec (nikola.komazec@htecgroup.com)
 * Created on: 09/11/2020
 */
public class VehicleGraphQLStepDef {

    @Autowired
    private VehicleProxy vehicle;

    @Given("^Set Vehicle$")
    public void setVehicle() {
        final SetVehicleResponse setVehicleResponse = vehicle.setVehicle();
        final String vehiclePlate = setVehicleResponse.getSetVehicle().getSetVehicleData().getVehicles().get(0).getLicensePlateNumber();
    }
}
