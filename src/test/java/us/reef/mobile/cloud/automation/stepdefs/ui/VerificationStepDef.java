package us.reef.mobile.cloud.automation.stepdefs.ui;

import io.cucumber.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;
import us.reef.mobile.cloud.automation.ui.screens.VerificationScreen;

import static org.assertj.core.api.Assertions.assertThat;

public class VerificationStepDef {

    @Autowired
    private VerificationScreen verification;

    @When("^Enter(?:s|ed)? verification code$")
    public void enterVerificationCode() {
        assertThat(verification.isVerificationScreenDisplayed())
                .as("Verification screen is not displayed!")
                .isTrue();
        verification.enterVerificationCode("1234");
        verification.tapOnContinueButton();
    }
}